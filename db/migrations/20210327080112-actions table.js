'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('taction', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      image_URL:{
        type:Sequelize.TEXT
      },
      status: {
        allowNull: false,
        type:Sequelize.BOOLEAN,
        defaultValue: true
      },
    });
     
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('taction');
  }
};