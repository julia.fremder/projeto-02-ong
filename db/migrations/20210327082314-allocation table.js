'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('tallocation', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      volunteer_name: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      volunteer_email: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      action_id:{
        type: Sequelize.INTEGER,
        references: { model: {tableName:'taction'}, key: 'id' },
        onDelete: 'CASCADE',
      }
    });
     
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tallocation');
  }
};