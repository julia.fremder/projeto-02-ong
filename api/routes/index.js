const {Router} = require('express');

const actionsRoutesV1 = require('./v1/actions.route');


module.exports = (app) => {

    const route = Router();

    route.route('/').get((request, response) => {
        response.send({você: 'está indo bem, continue...'});
    });

    actionsRoutesV1(route);
    
    app.use('/v1', route);
}