const actionsController = require('../../controllers/actions.controller');

module.exports = (route) => {


    route.route('/actions')
    .get(
        actionsController.getAllActions
        );

    route.route('/actions/:id')
    .get(
        actionsController.getActionById
        );

    route.route('/actions/:id/allocations')
    .post(
        actionsController.postAllocation
        );

    route.route('/actions/:id_allocation')
    .delete(
        actionsController.deleteAllocation
        );


}