const { actions, allocations } = require('../models')

const getAllActions = async (request, response) => {
    
    const result = await actions.findAll({})
    
     
    response.status(200).send(result.map((item)=> {

        const {id, description, image_URL} = item;

        return {

            id,
            description,
            image_URL

        }

    }) || [])
}


const getActionById = async (req, res) => {

  const {id} = req.params;

    try{
    const result = await actions.findOne({
       
      where: {
            id: id
        },
        include: {
          model: allocations,
          as: 'allocations',
        },
    });

    // console.log(result);
  
        res.status(200).send({
            id:result.id,
            description:result.description,
            image_URL: result.image_URL,
            allocations:result.allocations 
      
        });
      
      } catch (error) {
        console.log(error);

    res.status(500).send({ message: 'Internal server error!!' });
      }

    };




const postAllocation = async (req, res) => {

  

    try{
      
          //TODO: em qual atividade (id) será feita a inscrição?? pegar na rota.
          const {id} = req.params;
          
          console.log('action_id: ', id);
      
          //TODO: recuperar valores de inscricao para cadastro. pegar no no body da request.
          const body = req.body;
          console.log('body da allocation: ', body)
      
          //TODO: construir o model que será passado para o método create do sequelize que irá criar o insert no banco de dados e adicionar o registro. Esse model deve estar de acordo com a migration de criação da tabela inscrição.
          const model = {
            action_id: id, //atividade_id é o nome do id da atividade lá na tabela de inscricao, id é o nome do id da atividade na rota.
            volunteer_name: body.volunteer_name,
            volunteer_email: body.volunteer_email
          }
      
          //TODO: mandar para o banco de dados via sequelize
          await allocations.create(model);
      
          res.status(200).send('allocations')
      
    } catch (error) {
      
          res.status(500).send({message:'allocation was not concluded'})
      
    }
      
}


const deleteAllocation = async (req, res) => {

    try{
  
    //TODO: qual a inscricao deseja cancelar? pegar id na rota.
    const {id_allocation} = req.params;
  
    await allocations.destroy({
      
      where: {
      
        id: id_allocation,
      
      }
  
    });
  
    res.status(200).send({message: 'Deleted allocation ' + id_allocation})
  
  
    } catch (error) {
  
    res.status(500).send({message:'Allocation was not deleted'})
  
    }
  
  
  
  
  }
  

module.exports = {
    getAllActions,
    getActionById,
    postAllocation,
    deleteAllocation
}