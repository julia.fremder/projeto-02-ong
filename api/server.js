//imports
const express = require('express');
const cors = require('cors');
const routes = require('./routes/index');


//settings
const app = express();
const port = process.env.PORT || 3001;


//middlewares
app.use(express.json());
app.use(cors());

//routes
routes(app);



app.listen(port, ()=> {
    // console.log('Listening on port: ' + port);
})

module.exports = app;