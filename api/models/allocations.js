module.exports = (sequelize, DataTypes) => {

    const allocations = sequelize.define(
        "allocations", {
    
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false
    
            },
            volunteer_name: DataTypes.TEXT,
            volunteer_email:DataTypes.TEXT,
        },
        {
            undescored: true,
            paranoid: true,
            timestamps: false,
            tableName:"tallocation"
        })
    
        
        allocations.associate = function (models) {
            allocations.belongsTo(models.actions, {
              foreignKey: 'action_id',
              as: 'actions'
            });
          };
    



        return allocations;
    
    }