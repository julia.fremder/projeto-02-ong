

module.exports = (sequelize, DataTypes) => {

const actions = sequelize.define(
    "actions", {

        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            unique: true,
            allowNull: false

        },
        description: DataTypes.TEXT,
        image_URL:DataTypes.TEXT,
        status: DataTypes.BOOLEAN
    },
    {
        undescored: true,
        paranoid: true,
        timestamps: false,
        tableName:"taction"
    })


    actions.associate = function (models) {

        // we are adding the following now
        actions.hasMany(models.allocations, {
          foreignKey: 'action_id',
          as: 'allocations'
        });
      };

    return actions;

}